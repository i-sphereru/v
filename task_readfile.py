import requests
import bz2
from pathlib import Path
import logging as log

pass_www = 'https://проверки.гувм.мвд.рф/upload/expired-passports/list_of_expired_passports.csv.bz2'


def read_save_file(link):
    filename = link.split('/')[-1]
    print(filename)
    log.info(filename)
    r = requests.get(link, allow_redirects=True)
    open(filename, "wb").write(r.content)
    # read_save_file(pass_www)
    log.info("write file end")
    filename = pass_www.split('/')[-1]
    zipfile = bz2.BZ2File(filename)  # open the file
    data = zipfile.read()  # get the decompressed data
    newfilepath = filename[:-4]  # assuming the filepath ends with .bz2
    open(newfilepath, 'wb').write(data)  # write a uncompressed file
    log.info('write unzip file', newfilepath)
    target_filepath = '222.csv'
    Path(newfilepath).rename('/home/' + target_filepath)
    log.info('copy end ', target_filepath)
