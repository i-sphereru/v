# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from datetime import datetime
import logging as log
import sys

from cassandra.cluster import Cluster

from task_readfile import read_save_file, pass_www


def work_csv():
    KEYSPACE = "store"
    log.info('start')
    cluster = Cluster(['127.0.0.1'], port=9042)

    try:
        session = cluster.connect('store', wait_for_all_pools=True)
    except Exception as e:
        print("Not DB server")
        log.error(e)
        return -1
    log.info("setting keyspace . . .")
    session.set_keyspace(KEYSPACE)
    session.default_timeout = 2000

    log.info("creating table . . .")
    session.execute("""
        CREATE TABLE IF NOT EXISTS pass (
            PASS_SERIES int,
            PASS_NUM int,
            PRIMARY KEY(PASS_SERIES,PASS_NUM)
            );
            """)
    prepared = session.prepare("""
            INSERT INTO pass (PASS_SERIES,PASS_NUM)
            VALUES (?, ?)
            """)
    pass_num = 0
    pass_series = 0
    num_row =0
    with open("/home/222.csv", "r") as fares:
        for fare in fares:
            num_row=num_row+1;
            if (num_row % 1000000) == 0:
                log.info(f,'Lines {num_row') 
            columns = fare.split(",")
            if str(columns[0]).strip().isdigit():
                pass_series = int(columns[0])
            else:
                continue
            if str(columns[1]).strip().isdigit():
                pass_num = int(columns[1])
            else:
                continue
            probe=0
            while True:
                if probe >5:
                    log.error("Too many probe to insert to DB")
                    sys.exit(1)
                else:
                    probe= probe+1
                try:
                    session.execute(prepared, [pass_series, pass_num])
                    break
                except Exception as e:
                    log.error(e)
                    pass


    # closing the file
    fares.close()
    log.info('end read file')
    # closing Cassandra connection
    session.shutdown()
    log.info('close connection')


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    log.basicConfig(filename='myapp.log', level=log.DEBUG)
    log.warning('start main')
    print_hi('PyCharm')
    read_save_file(pass_www)
    work_csv()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
